(function() {

    'use strict';

    describe('When using the history logger service', function() {

        var historyLogger;

        beforeEach(module('simpleCalc'));

        beforeEach() {
            // inject an instance of the history logger service
        }));

        it('should add a new message to its history', function() {
            // test that calling the .log() function on the history logger
            // service will add an item to its history
        });

        it('should clear its history', function() {
            // test that calling the .clear() function on the history logger
            // will remove all items from its history
        });
        
    });

})();
