
exports.config = {
    
    directConnect: true,

    suites: {
        homepage: 'spec.js'
    },

    capabilities: {
      browserName: 'chrome'
    },

    framework: 'jasmine2'

};
