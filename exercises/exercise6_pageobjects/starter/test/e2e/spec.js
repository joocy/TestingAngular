(function() {
    
    'use strict';

    describe('Simple Calculator application', function() {


        var SimpleCalcPage = function() {
            // define attributes for 
            //      firstNumber
            //      secondNumber
            //      result
            //      goButton
            //      history
            
            // define a function called start which navigates to the app 
            

            // this is a helper function to return an option element based on
            // the operand string that it displays
            function get_option_for_symbol(s) {
                return element(by.cssContainingText('option', s));
            }

            // define a function called doCalculationWith which takes there arguments:
            //      - the value of the first number
            //      - the value of the second number
            //      - (optional) string operand to use (e.g. '*')
            // the function will set the values for firstNumber and secondNumber
            // if an operand has been passed, find the right option element and select it
            // then click the goButton

            // define a function called lastResult which returns
            // the text in the result element

            // define a function called historySize that returns
            // the count of items in the history element
        };

        // rewrite the rest of the spec to use the new SimpleCalcPage

        beforeEach(function() {
            browser.get('http://localhost:8080');
        });

        it('should have a title', function() {
            expect(browser.getTitle()).toEqual('Simple Calculator');
        });

        it('should add numbers', function() {
            firstNumber.sendKeys(1);
            secondNumber.sendKeys(2);
            goButton.click();
            expect(result.getText()).toEqual('3');
        });

        it('should use the selected operand', function() {
            firstNumber.sendKeys(2);
            secondNumber.sendKeys(3);
            element(by.cssContainingText('option', '*')).click();
            goButton.click();
            expect(result.getText()).toEqual('6');
        });

        it('should display the result history', function() {
            firstNumber.sendKeys(2);
            secondNumber.sendKeys(3);
            goButton.click();
            expect(history.count()).toBe(1);
        });

    });

})();
