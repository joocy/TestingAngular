Testing Angular Applications
============================

This repo contains the slide deck and exercises for the Testing Angular course delivered by Instil.

Slides
------
The slides are built using reveal.js and are viewable in any browser. The zip file in the slides folder contains all the files necessary to view the presentation offline. An online version, allowing for a synced speaker view, can be made available if required.

Exercises
---------
The exercises in this course are designed not to require any specific editor or IDE. The files can be edited directly or imported into an IDE of the trainer/trainee's choosing. All exercises require `npm` and `bower` be installed. Except where specified in any exercise, the `package.json` and `bower.json` files delcare all required dependencies.

Running tests
-------------
To run the unit tests in the exercises, simply use the `npm test` command from the command line. This will also install any dependencies required. Dependencies can be installed per exercise by running the `npm install` command.

The end-to-end tests have purposefully not been setup to be run by npm. To run those, you must use protractor on the commandline with `protractor test/e2e/conf.js`.
